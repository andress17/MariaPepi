<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Problemas extends Model
{
    public $timestamps = false;

    public function envios()
    {
        return $this->hasMany(Envios::class);
    }
}


