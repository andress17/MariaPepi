<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Problemas;
use App\Envios;
//importa tota la DB
use DB;
//importa el model User, equivalent a la taula users
use App\User;

class ProblemaController extends Controller
{
    public function add()
    {
    	return view('create');
    }

    public function create(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:pdf|max:2048',
        ]);

        $problema = new Problemas();

        $problema->titulo = $request->titulo;
        $problema->puntuacion = $request->puntos;

        $fileName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $fileName);

        $problema->pdf = $fileName;

        $problema->save();
        return redirect('/');
    }

    public function verProblemas()
    {
        $problemas = Problemas::all();

        return view('ver_problemas', compact('problemas'));
    }
}
