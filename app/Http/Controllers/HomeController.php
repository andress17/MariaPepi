<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Problemas;
use App\Envios;
//importa tota la DB
use DB;
//importa el model User, equivalent a la taula users
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function ranking()
    {
        $users = User::orderBy('puntuacion', 'DESC')->get();
        return view('ranking', compact('users'));
    }
}
