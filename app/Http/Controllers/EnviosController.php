<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Problemas;
use App\Envios;
//importa tota la DB
use DB;
//importa el model User, equivalent a la taula users
use App\User;

class EnviosController extends Controller
{
    public function addSend(int $problema)
    {
        $p = Problemas::Where('id','=',$problema)->first();
        return view('add_submit', compact('p'));
    }

    public function sendCode(int $p, User $user, Request $request)
    {
        // $request->validate([
        //     'file' => 'required|mimes:text/x-java-source|max:2048',
        // ]);

        $fileName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $fileName);

        $problema = Problemas::Where('id','=',$p)->first();
        $envio = new Envios();

        $envio->user = $user->id;
        $envio->problema = $problema->id;

        $envio->path = $fileName;

        $envio->save();
        redirect("/");
    }

     public function verEnvios()
     {
        $envios = Envios::all();

        return view('ver_envios', compact('envios'));
     }

     public function aceptar(int $envio)
     {
        $e = Envios::Where('id','=',$envio)->first();
        $e->estado = 'A';

        $user = User::Where('id', '=', $e->user)->first();

        $problema = Problemas::Where('id', '=', $e->problema)->first();

        $user->puntuacion += $problema->puntuacion;
        $user->resueltos++;

        $e->save();
        $user->save();
        redirect("/");
    }

    public function rechazar(int $envio)
    {
       $e = Envios::Where('id','=',$envio)->first();
       $e->estado = 'R';

       $e->save();
       redirect("/");
   }
}
