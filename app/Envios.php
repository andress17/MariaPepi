<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Envios extends Model
{
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function problemas()
    {
        return $this->belongsTo(Problemas::class);
    }
}

