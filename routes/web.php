<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/add', 'ProblemaController@add');
Route::post('/add', 'ProblemaController@create');

Route::get('/send/{problemas}','EnviosController@addSend');
Route::post('/send/{int}/{user}','EnviosController@sendCode');

Route::get('/ver_problemas', 'ProblemaController@verProblemas');

Route::get('/ver_envios', 'EnviosController@verEnvios');
Route::get('/aceptar/{problemas}','EnviosController@aceptar');
Route::get('/rechazar/{problemas}','EnviosController@rechazar');

Route::get('/ranking','HomeController@ranking');

