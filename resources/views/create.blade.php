@extends('layouts.app')

@section('content')

    @if (Auth::check() && Auth::user()->profesor)
        <h2>Crear un problema</h2>

        <form action="/add" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="titulo">Titulo</label>
                <input type="text" name="titulo" id="titulo" placeholder="Titulo">
            </div>

            <div class="form-group">
                <label for="puntos">Puntos</label>
                <input type="number" name="puntos" id="puntos" placeholder="Puntos">
            </div>

            <input type="file" name="file" class="form-control">

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Añadir problema</button>
            </div>
            {{ csrf_field() }}
        </form>
    @else
        <h2>No eres un profesor</h2>
    @endif
@endsection
