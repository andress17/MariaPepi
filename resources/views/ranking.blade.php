@extends('layouts.app')

@section('content')

    @if (Auth::check())
        <h2>Listado de usuarios</h2>

        <table class="table">
            <thead>
                <tr>
                    <th>Nombre de usuario</th>
                    <th>Puntos</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            {{$user->name}}
                        </td>
                        <td>
                            {{$user->puntuacion}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ csrf_field() }}
    @endif

@endsection
