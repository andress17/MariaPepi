@extends('layouts.app')

@section('content')

    @if (Auth::check())
        <h2>Listado de problemas</h2>

        <table class="table">
            <thead>
                <tr>
                    <th colspan="2">Problemas</th>
                </tr>
            </thead>
            <tbody>
                @foreach($problemas as $problema)
                    <tr>
                        <td>
                            {{$problema->titulo}}
                        </td>
                        <td>
                            {{$problema->puntuacion}}
                        </td>
                        <td>
                            <a href="/send/{{$problema->id}}">Enviar solucion</a>
                        </td>
                        <td>
                            <a href="/uploads/{{$problema->pdf}}">Descargar Problema</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ csrf_field() }}
    @endif

@endsection
