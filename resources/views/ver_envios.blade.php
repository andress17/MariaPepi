@extends('layouts.app')

@section('content')

    @if (Auth::check() && Auth::user()->profesor)
        <h2>Listado de envios</h2>

        <table class="table">
            <thead>
                <tr>
                    <th>Problema</th>
                    <th>Usuario</th>
                    <th>Codigo</th>
                    <th>Aceptar</th>
                    <th>Rechazar</th>
                </tr> 
            </thead>
            <tbody>
                @foreach($envios as $envio)
                    @if ($envio->estado == 'E')
                        <tr>
                            <td>
                                {{$envio->problema}}
                            </td>
                            <td>
                                {{$envio->user}}
                            </td>
                            <td>
                                <a href="/uploads/{{$envio->path}}">Descargar codigo</a>
                            </td>
                            <td>
                                <a href="/aceptar/{{$envio->id}}">Aceptar</a>
                            </td>
                            <td>
                                <a href="/rechazar/{{$envio->id}}">Rechazar</a>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        {{ csrf_field() }}
    @else
        <h2>No eres un profesor</h2>
    @endif

@endsection
