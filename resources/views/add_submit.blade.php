@extends('layouts.app')

@section('content')

    @if (Auth::check())
        <h2>Enviar codigo</h2>

        <form action="/send/{{ $p->id }}/{{Auth::user()->id}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <label for="code">Codigo:</label>
            <input type="file" name="file" class="form-control">

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Añadir problema</button>
            </div>
        </form>
    @endif

@endsection
